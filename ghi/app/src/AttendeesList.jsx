

function AttendeesList(props) {
    return (
        <>
            <table className="table table-light table-striped w-100">
                <thead className="">
                    <tr>
                        <th>Name</th>
                        <th>Conference</th>
                    </tr>
                </thead>
                <tbody className="table-group-divider">
                    {props.attendees.map(attendee => {
                        return (
                            <tr key={attendee.href}>
                                <td>{ attendee.name }</td>
                                <td>{ attendee.conference }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}
  
export default AttendeesList;