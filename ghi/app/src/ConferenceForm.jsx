import React, {useEffect, useState} from 'react';

function ConferenceForm () {
    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
    
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }
    const [name, setName] = useState('')
    function handleNameChange(event){
        const value = event.target.value;
        setName(value);
    }
    const [starts, setStarts] = useState('')
    function handleStartsChange(event){
        const value = event.target.value;
        setStarts(value)
    }
    const [ends, setEnds] = useState('')
    function handleEndsChange(event){
        const value = event.target.value;
        setEnds(value);
    }
    const [description, setDescription] = useState('')
    function handleDescriptionChange(event){
        const value = event.target.value;
        setDescription(value);
    }
    const [max_presentations, setMaxPresentations] = useState('')
    function handleMaxPresentationsChange(event){
        const value = event.target.value;
        setMaxPresentations(value);
    }
    const [max_attendees, setMaxAttendees] = useState('')
    function handleMaxAttendeesChange(event){
        const value = event.target.value;
        setMaxAttendees(value);
    }
    const [location, setLocation] = useState('')
    function handleLocationChange(event){
        const value = event.target.value;
        setLocation(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.starts = starts;
        data.name = name;
        data.ends = ends;
        data.description = description;
        data.max_presentations = parseInt(max_presentations);
        data.max_attendees = parseInt(max_attendees);
        data.location = location;
        console.log(data)
        
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                const newConference = await response.json();
                console.log(newConference);
                setName('');
                setStarts('');
                setEnds('');
                setDescription('');
                setMaxPresentations('');
                setMaxAttendees('');
                setLocation('')
            } else {
                console.error(`Error: ${response.status} ${response.statusText}`);
            }
        } catch (error) {
            console.error('Fetch error:', error);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input placeholder="Name" onChange={handleNameChange} value={name} type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Starts" onChange={handleStartsChange} value={starts} type="date" name="starts" id="starts" className="form-control" />
                            <label htmlFor="starts">Start Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Ends" onChange={handleEndsChange} value={ends} type="date" name="ends" id="ends" className="form-control" />
                            <label htmlFor="ends">End Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea placeholder="Description" onChange={handleDescriptionChange} value={description} type="textarea" name="description" id="description" className="form-control"></textarea>
                            <label htmlFor="description">Description</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Max-pres" onChange={handleMaxPresentationsChange} value={max_presentations} type="number" name="max_presentations" id="max-pres" className="form-control" />
                            <label htmlFor="max_presentations">Max Presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Max-atten" onChange={handleMaxAttendeesChange} value={max_attendees} type="number" name="max_attendees" id="max-atten" className="form-control" />
                            <label htmlFor="max_attendees">Max Attendees</label>
                        </div>
                        <div className="mb-3">
                            <select required onChange={handleLocationChange} value={location} id="location" name="location" className="form-select">
                                <option>Choose a location</option>
                                    {locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.name}
                                            </option>
                                        );
                                    })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ConferenceForm;