import React, {useEffect, useState} from 'react';

function PresentationForm () {
    const [conferences, setConferences] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
    
        if (response.ok) {
            const data = await response.json();
            console.log("lable:", data)
            setConferences(data.conferences)
        }
    }
    const [name, setName] = useState('')
    function handleNameChange(event){
        const value = event.target.value;
        setName(value);
    }
    const [email, setEmail] = useState('')
    function handleEmailChange(event){
        const value = event.target.value;
        setEmail(value)
    }
    const [company, setCompany] = useState('')
    function handleCompanyChange(event){
        const value = event.target.value;
        setCompany(value);
    }
    const [description, setDescription] = useState('')
    function handleDescriptionChange(event){
        const value = event.target.value;
        setDescription(value);
    }
    const [title, setTitle] = useState('')
    function handleTitleChange(event){
        const value = event.target.value;
        setTitle(value);
    }
    const [conference, setConference] = useState('')
    function handleConferenceChange(event){
        const value = event.target.value;
        setConference(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.title = title;
        data.presenter_name = name;
        data.company_name = company;
        data.synopsis = description;
        data.presenter_email = email;
        data.conference = conference;
        console.log(data)

        const presentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(presentationUrl, fetchConfig);
            if (response.ok) {
                const newPresentation = await response.json();
                console.log(newPresentation);
                setName('');
                setTitle('');
                setCompany('');
                setDescription('');
                setEmail('');
                setConference('')
            } else {
                console.error(`Error: ${response.status} ${response.statusText}`);
            }
        } catch (error) {
            console.error('Fetch error:', error);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    // Hey run once as the page loads but then don't run until this is here
    useEffect(() => {
        console.log('conf:', conferences)
    }, [conferences]);    //<======== here
    
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input placeholder="Name" onChange={handleNameChange} value={name} type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name of Presenter</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Email" onChange={handleEmailChange} value={email} type="email" name="email" id="email" className="form-control" />
                            <label htmlFor="email">Presenter Email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Company" onChange={handleCompanyChange} value={company} type="text" name="company" id="company" className="form-control" />
                            <label htmlFor="company">Presenter Company</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Title" onChange={handleTitleChange} value={title} type="text" name="title" id="title" className="form-control" />
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea placeholder="Description" onChange={handleDescriptionChange} value={description} type="textarea" name="description" id="description" className="form-control"></textarea>
                            <label htmlFor="description">Description</label>
                        </div>
                        <div className="mb-3">
                            <select required onChange={handleConferenceChange} value={conference} id="conference" name="conference" className="form-select">
                                <option>Choose a conference</option>
                                    {conferences.map(conference => {
                                        return (
                                            <option key={conference.id} value={conference.id}>
                                                {conference.name}{conference.id}
                                            </option>
                                        );
                                    })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default PresentationForm;