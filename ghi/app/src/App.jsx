import Nav from './Nav.jsx'
import AttendeesList from './AttendeesList.jsx'
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm.jsx'
import AttendConferenceForm from './AttendConferenceForm.jsx'
import PresentationForm from './PresentationForm.jsx'
import MainPage from './MainPage.jsx'
import { BrowserRouter, Routes, Route } from "react-router-dom";


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container m-3">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/attendees/new" element={<AttendConferenceForm />} />
          <Route path="/presentations/new" element={<PresentationForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;